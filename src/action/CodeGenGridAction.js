import {Dispatcher} from 'simplr-flux';
import Constants from '../Constants';
import * as APIService from "../Service";

/**
* GC_PC_05
* call postCallGateway() in Service.js with the parameter gridLoadURL with the query page number from the Constants,
* searchFilterData,and then a call back function 
* GC_PC_09
* Dispatch response to store.
*/
const gridLoad = (loadGridPostData ) => {
    try{
        console.log("asdasd");
        APIService.postCallGateway(
            Constants.URL.gridLoadURL,
            loadGridPostData,
            (data )=>
            {
                if(data.success)
                {
                    if(data.response!="" && data.response)
                    {
                        let contentdata={ type:Constants.URL.gridLoadURL,
                                          response:data.response
                                        }
                                        Dispatcher.dispatch(contentdata);
                    }
                }
            }
        );
    }
    catch(error)
    {
        console.log(error.message);
    }
}






/**
 * GC_PC_40
 *  call getCallGateway() in Service.js with the parameter filterLoadURL from the Constants,
 *  searchFilterData, and then a call back function
 * GC_PC_44
 *  Dispatch response to store.
 */
const filterDataLoad = () => {
    try{
        APIService.getCallGateway(
            Constants.URL.filterLoadURL,            
            (data )=>
            {
                if(data.success)
                {
                    if(data.response!="" && data.response)
                    {
                        let contentdata={ type:Constants.URL.filterLoadURL,
                                          response:JSON.parse(data.response)
                                        }
                                        console.log(contentdata);
                                        Dispatcher.dispatch(contentdata);
                    }
                }
            }
        );
    }
    catch(error)
    {
        console.log(error.message);
    }
}



export default { gridLoad   , filterDataLoad }