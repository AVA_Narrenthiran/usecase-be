import EventEmitter from 'events';
import { Dispatcher } from 'simplr-flux';
import Constants from '../Constants';


class Store extends EventEmitter
{
    Response ='';
    /**
     * FC_PC_7 , FC_PC_18 , FC_PC_29
     * GC_PC_10, GC_PC_23, GC_PC_45
     * handleStore() will get invoked based on the URL
     * the switch case will get triggered and emit the response 
     */
    handleStore=(contentdata )=>{
        console.log(contentdata.action.type);
        switch (contentdata.action.type) {
            
            case Constants.GO.CodeGenURL:{
                this.Response=contentdata.action.response;
                /**
                 * GC_PC_11
                 * After checking the URL and getting the response 
                 * it will emit a key word to the formcomponents.jsx
                 */
                console.log(this.Response);
                this.emit('codegengo');
                break;
            }

            case Constants.URL.outputFileNameUpdate:{
                this.Response=contentdata.action.response;
                /**
                 * GC_PC_11
                 * After checking the URL and getting the response 
                 * it will emit a key word to the formcomponents.jsx
                 */
                console.log(this.Response);
                this.emit('outputfileupdate');
                break;
            }

            case Constants.URL.gridLoadURL:{
                this.Response=contentdata.action.response;
                /**
                 * GC_PC_11
                 * After checking the URL and getting the response 
                 * it will emit a key word to the formcomponents.jsx
                 */
                console.log(this.Response);
                this.emit('loadgrid');
                break;
            }

            case Constants.URL.filterLoadURL:{
                this.Response=contentdata.action.response;
                /**
                 * GC_PC_46
                 * After checking the URL and getting the response 
                 * it will emit a key word to the formcomponents.jsx
                 */
                this.emit('loadFilter');
                break;
            }
            
            case Constants.URL.formLoadURL:{
                this.Response=contentdata.action.response; 
                console.log(this.Response);
                this.emit('dynamicData');
                break;
            }
            case Constants.URL.formPostDataURL:{
                this.Response=contentdata.action.response;
                this.emit('postData');
                break;
            }
     
            case Constants.URL.formGetDataURL:{
                this.Response=contentdata.action.response;                
                this.emit('getDetails');
                break;
            }

            
        }
    }
}

const StoreObj =new Store() ;
Dispatcher.register(StoreObj.handleStore.bind(StoreObj));

export default StoreObj ;